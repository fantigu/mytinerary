import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';

import logo from './MYtineraryLogo.png';
import home from './homeIcon.png';
import right from './circled-right-2.png';

var buttonStyle = {
    display: 'block',
};

class App extends React.Component{

componentDidMount() {
    const script = document.createElement("script");
    script.async = true;
    script.src = "https://kit.fontawesome.com/874269423f.js";
    script.crossorigin = "anonymous";
    document.head.appendChild(script);
  }

    render(){
        
        var settings = {
            arrows: true,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        };

        return (
            <main>
                
                <nav>
                    <div class="dropdown" id="menu">
                        <i class="fas fa-bars fa-3x"></i>
                        <div class="dropdown-content-menu">
                            <ul>
                                <li>
                                    <p>Opción 1</p>
                                    <p>Opción 2</p>
                                    <p>Opción 3</p>
                                    <p>Opción 4</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="dropdown" id="login">
                        <i class="fas fa-user-circle fa-3x"></i>
                        <div class="dropdown-content-login">
                            <ul>
                                <li>
                                    <p>Ingresar</p>
                                    <p>Registrarme</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                
                <header></header>
                
                <section>
                    <h2>Find your perfect trip, designed by insiders who know and love their cities</h2>
                    <img src={right} width="200px"/>
                </section>
                
                <section>
                    <h2>Popular MYtineraries</h2>
            
                    <div id="galeria">
                        <Slider {...settings}>
                            <div>
                                <div class="columnaizq">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/barcelona.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Barcelona</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/amsterdam.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Amsterdam</h3>
                                    </div>
                                </div>
                                <div class="columnader">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/newyork.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>New York</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/paris.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Paris</h3>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="columnaizq">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/buenosaires.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Buenos Aires</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/tokyo.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Tokyo</h3>
                                    </div>
                                </div>
                                <div class="columnader">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/moscow.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Moscow</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/sofia.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Sofia</h3>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="columnaizq">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/liverpool.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Liverpool</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/caracas.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Caracas</h3>
                                    </div>
                                </div>
                                <div class="columnader">
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/madrid.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Madrid</h3>
                                    </div>
                                    <div class="ciudad">
                                        <img src={require ('./imagenes/sydney.jpg')} width="200px"/>
                                    </div>
                                    <div>
                                        <h3>Sydney</h3>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </Slider>
                        
                    </div>
                    
                </section>

            </main>
            
        );
        
    }
};

export default App;